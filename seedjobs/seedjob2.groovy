pipelineJob('pipeline1_converted') {

  description('')

  displayName('pipeline1_converted')

  keepDependencies(false)

  quietPeriod(0)

  checkoutRetryCount(0)

  disabled(false)

  concurrentBuild(false)

  configure { flowdefinition ->

    flowdefinition / 'actions' << 'org.jenkinsci.plugins.pipeline.modeldefinition.actions.DeclarativeJobAction'(plugin:'pipeline-model-definition@2.2075.vce74e77b_ce40')

    flowdefinition / 'actions' << 'org.jenkinsci.plugins.pipeline.modeldefinition.actions.DeclarativeJobPropertyTrackerAction'(plugin:'pipeline-model-definition@2.2075.vce74e77b_ce40') {

      'jobProperties'()

      'triggers'()

      'parameters'()

      'options'()

    }

    flowdefinition << delegate.'definition'(class:'org.jenkinsci.plugins.workflow.cps.CpsFlowDefinition',plugin:'workflow-cps@2683.vd0a_8f6a_1c263') {

      'script'('''pipeline {
            agent any
            stages {
                  stage(\'pull\') {
                        steps {
                              git branch: \'main\', url: \'https://gitlab.com/phuntru/amol.git\'
                        }
                  }
                  stage(\'Build\') {
                        steps {
                               echo \'build successful\'
                        }
                  }
                  stage(\'Test\') {
                        steps {
                              echo \'test successful\'
                        }
                  }
                  stage(\'Deploy\') {
                        steps {
                               echo \'deploy successful\'
                        }
                  }
            }
      }''')

      'sandbox'(true)

    }

  }

}